package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);

        try {
            Image image = ImageIO.read(url);
            System.out.println(url);
            System.out.println(image);
            if (width == image.getWidth(null) && height == image.getHeight(null)) {
                this.image = image;
            } else {
                this.image = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private class ImageWorker extends SwingWorker<Image, Void> {
        int x;
        int y;
        int deltaX;
        int deltaY;
        int width;
        int height;
        String url;

/*        protected ImageWorker(int x, int y, int deltaX, int deltaY, int width, int height) {
            this.x = x;
            this.y = y;
            this.deltaX = deltaX;
            this.deltaY = deltaY;
            this.width = width;
            this.height = height;
            this.url = "http://via.placeholder.com/" + this.width + "x" + this.height;
        }*/

        protected ImageWorker(ImageShape imageShape, int x, int y, int width, int height, String url) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.url = url;
        }

        @Override
        protected Image doInBackground() {
            try {
                Image image = ImageIO.read(url);

                if (width == image.getWidth(null) && height == image.getHeight(null)) {
                    image = image;
                } else {
                    image = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return image;
        }

        @Override
        protected void done() {
            try {
                image = get();

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void paint(Painter painter) {

        painter.drawImage(this.image, fX, fY, fWidth, fHeight);

    }
}
