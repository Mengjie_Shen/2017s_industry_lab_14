package ictgradschool.industry.lab14.ex01;

import javax.swing.*;

/**
 * Created by Mengjie
 * Date : 2018/1/7
 * Time : 13:34
 **/
public class ProgressBarDemo extends JPanel {
    private static void createAndShowGUI() {
// Create and set up the window.
        JFrame frame = new JFrame("ProgressBarDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

// Create and set up the content pane.
        JComponent newContentPane = new ProgressBarDemo();
        frame.add(newContentPane);

// Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
// Schedule a job for the event-dispatching thread:
// creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

