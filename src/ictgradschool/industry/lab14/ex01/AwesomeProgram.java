package ictgradschool.industry.lab14.ex01;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

public class AwesomeProgram implements ActionListener {
    private JLabel progressLabel = new JLabel();
    private JLabel myLabel = new JLabel();
    private JButton myButton = new JButton();


    /**
     * Called when the button is clicked.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        myButton.setEnabled(false);
        // Start the SwingWorker running
        MySwingWorker worker = new MySwingWorker();
        worker.execute();

    }

    private class MySwingWorker extends SwingWorker<Integer, Void> {
        int result = 0;
        protected Integer doInBackground() throws Exception {
            for (int i = 0; i < 100; i++) {
                // Do some long-running stuff
                result += doStuffAndThings();
                // Report intermediate results
                progressLabel.setText("Progress: " + i + "%");
            }
            return result;
        }

        private int doStuffAndThings() {
            return 10;
        }

        @Override
        protected void done() {
            try {
                result = get();
                myButton.setEnabled(true);
                myLabel.setText("Result: " + result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }
    }
}
