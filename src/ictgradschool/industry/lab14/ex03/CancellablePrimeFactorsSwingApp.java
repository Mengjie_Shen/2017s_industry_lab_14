package ictgradschool.industry.lab14.ex03;

import ictgradschool.industry.lab14.ex02.PrimeFactorsSwingApp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/**
 * Created by Mengjie
 * Date : 2018/1/7
 * Time : 17:04
 **/
public class CancellablePrimeFactorsSwingApp extends JPanel{
    private final JTextField tfN;
    private JButton _startBtn;        // Button to start the calculation process.
    private JButton _abortBtn;
    private JTextArea _factorValues;  // Component to display the result.
    private CancellablePrimeFactorisationWorker worker;

    public CancellablePrimeFactorsSwingApp () {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        tfN = new JTextField(20);

        _startBtn = new JButton("Compute");
        _abortBtn = new JButton("Abort");
        _abortBtn.setEnabled(false);
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                long n = 0;

                try {
                    n = Long.parseLong(strN);
                } catch(NumberFormatException e) {
                    System.out.println(e);
                }

                worker = new CancellablePrimeFactorisationWorker(n);
                worker.execute();

            }
        });
        _abortBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                worker.cancel(true);
                worker = null;
                _startBtn.setEnabled(true);
                _abortBtn.setEnabled(false);
            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_abortBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500,300));
    }


//    private void cancel() {
//        worker.cancel(true);
//    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new CancellablePrimeFactorsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }


        });
    }

    private class CancellablePrimeFactorisationWorker extends SwingWorker<java.util.List<Long>, Void> {
        private long N;

        private CancellablePrimeFactorisationWorker(long N) {
            this.N = N;
        }

        @Override
        protected java.util.List<Long> doInBackground() throws Exception {
            java.util.List<Long> result = new java.util.ArrayList<>();

            _startBtn.setEnabled(false);
            _abortBtn.setEnabled(true);
            _factorValues.setText(null);
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            for (long i = 2; i*i <= N && !isCancelled(); i++) {

                while (N % i == 0 && !isCancelled()) {
                    result.add(i);
                    N = N / i;
                }
            }

            if (N > 1 && !isCancelled()) {
                result.add(N);
            }



            setCursor(Cursor.getDefaultCursor());
            return result;
        }


        @Override
        protected void done() {
            try {
                List<Long> result = get();
                for (int i = 0; i < result.size(); i++) {
                    _factorValues.append(result.get(i) + "\n");
                }
                _startBtn.setEnabled(true);
                _abortBtn.setEnabled(false);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            } catch (ExecutionException e) {
                System.out.println(e.getMessage());
            } catch (CancellationException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
